import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'
var accounting = require("accounting");


export default{
	namespaced: true,
	state:{
		artpromomail:[],
	},

	mutations:{
		ARTPROMOMAIL(state, value){
			state.artpromomail = value
		},

		
	},

	actions:{
		//Función para cargar los avisos de
		saveArtPromo({commit, dispatch}, artpromomail){
      var url = Vue.http.options.root.replace("tienda3", "verart/")

			var array = []
			artpromomail.forEach((element)=>{
				array.push({
					url: element.Url,
					descrip: element.Descrip,
					preciopub: accounting.formatNumber(element.Preciopub, 2), 
					pjedesc: element.Pjedesc,
					numart: element.Numart.trim(),
					link: url + element.Numart.trim()
				})
			})
			commit('ARTPROMOMAIL',array)
		},
		
  },

	getters:{
		getArtPromoMail(state){
		  return state.artpromomail
		},

		
	}
}