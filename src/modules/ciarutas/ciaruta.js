import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'

export default{
	namespaced: true,
	state:{
		ciaruta:[],
		leeUserweb: {}
	},

	mutations:{
		CIARUTA(state, value){
			state.ciaruta = value
		},


		UPDATE_LEEUSERWEB(state, leeUserweb){
			state.leeUserweb = leeUserweb
		}
		
	},

	actions:{

		updateLeeuser ({dispatch, commit}, leeUserweb){
			console.log('updateLeeuser', leeUserweb)
			commit('UPDATE_LEEUSERWEB', leeUserweb)
		},

		//Función para cargar los avisos de
		traerCiaruta({commit, dispatch}, payload){

			//retornar una promesa (resolve, rejecet)
			return new Promise((resolve, reject) => {
			  Vue.http.get('auth/api/v1/getciarutaslist')
				.then(respuesta=>{return respuesta.json()})
				.then(respuestaJson=>{
					// console.log('Productos', respuestaJson)
					if(respuestaJson == null){
						resolve(null) 
					}else{
		      	commit('CIARUTA',respuestaJson)
		      	resolve(true) 
					}
		    }, error => {
        	reject(error)
	      	})
	      })
		},
		
	},

	getters:{
		getCiaruta(state){
		  return state.ciaruta
		},
		getLeeuserweb (state){
			return state.leeUserweb
		}
	}
}